﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles
{
	class Vehicle
	{
		private string _make;
		private double _mileage;
		private double _speed;
		private double _fuelCapacity;

		public Vehicle(string make)
		{
			//Initialize ALL field variables
			_make = make;
			_mileage = 0;
			_speed = 0;
			_fuelCapacity = 0;
		}
	
		public virtual void Go()
		{
			Console.WriteLine("Base vehicle going...");
		}
	}
}
